<?php include('inc/_functions.php'); ?>
<?php include('inc/_variables.php'); ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
<title>Basic 89</title>
<meta charset="iso-8859-1">
<link rel="stylesheet" href="styles/layout.css" type="text/css">
<!--[if lt IE 9]><script src="scripts/html5shiv.js"></script><![endif]-->
</head>
<body>
<div class="wrapper row1">
  <header id="header" class="clear">
    <div id="hgroup">
      <h1><a href="#"><?php echo $titulo; ?></a></h1>
      <h2><?php echo $subtitulo; ?></h2>
    </div>
    <nav>
      <ul>
        <li><a href="#"><?php echo $primeiro_link; ?></a></li>
        <li><a href="#"><?php echo $segundo_link; ?></a></li>
        <li><a href="#"><?php echo $terceiro_link; ?></a></li>
        <li><a href="#"><?php echo $quarto_link; ?></a></li>
        <li class="last"><a href="#"><?php echo $quinto_link; ?></a></li>
      </ul>
    </nav>
  </header>
</div>

<!-- content -->
<?php include('inc/paginas/inicial.php'); ?>

<!-- Footer -->
<div class="wrapper row3">
  <footer id="footer" class="clear">
    <p class="fl_left"><?php echo $rodape; ?></p>
    <p class="fl_right">Template by <a href="http://www.os-templates.com/" title="Free Website Templates">OS Templates</a></p>
  </footer>
</div>
</body>
</html>
