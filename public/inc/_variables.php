<?php

/**
 * Neste arquivo serão declaradas todas as variáveis que serão
 * exibidas na nossa página principal.
 */

 $titulo = "Curso de PHP";
 $subtitulo = "Introdução e Prática";
 $primeiro_link = "Inicial";
 $segundo_link = "Sobre o PHP";
 $terceiro_link = "Conteúdo";
 $quarto_link = "Professores";
 $quinto_link = "Contato";

 $date = date('Y');
 $rodape = "Copyright © ${date} - Todos os direitos reservados - fuctura.com.br";
